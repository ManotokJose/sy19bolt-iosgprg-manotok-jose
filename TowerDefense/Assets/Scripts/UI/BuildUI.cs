﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildUI : MonoBehaviour
{
    ResourceController Resources;

    public GameObject TowerBuildSlot;

    public GameObject SingleTarget;
    public GameObject AreaOfEffect;

    public GameObject PowerButton;
    public GameObject SingleTargetButton;
    public GameObject AreaOfEffectButton;

    // Start is called before the first frame update
    void Start()
    {
        Resources = ResourceController.Instance;
    }

    // Update is called once per frame
    void Update()
    {
    }

    //power on
    public void ToggleSelection()
    {
        if (TowerBuildSlot.activeInHierarchy == false)
        {
            return;
        }

        SingleTargetButton.SetActive(!SingleTargetButton.activeSelf);
        AreaOfEffectButton.SetActive(!AreaOfEffectButton.activeSelf);
    }

    public void BuildTower(string id)
    {
        if (Resources.Energy < 10)
        {
            return;
        }

        if (id == "1")
        {
            if (TowerBuildSlot == null)
            {
                return;
            }
            Instantiate(SingleTarget, TowerBuildSlot.transform.position, Quaternion.identity);
            ToggleSelection();
        }

        if (id == "2")
        {
            if (TowerBuildSlot == null)
            {
                return;
            }
            Instantiate(AreaOfEffect, TowerBuildSlot.transform.position, Quaternion.identity);
            ToggleSelection();
        }
    }

    public void SetTowerLocation(GameObject TowerLocation)
    {
        TowerBuildSlot = TowerLocation;
    }
    
}
