﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    ResourceController Resources;

    public GameObject Attack;
    public GameObject Health;
    public GameObject Defense;

    // Start is called before the first frame update
    void Start()
    {
        Resources = ResourceController.Instance;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ToggleSelection()
    {
        Attack.SetActive(!Attack.activeSelf);
        Health.SetActive(!Health.activeSelf);
        Defense.SetActive(!Defense.activeSelf);
    }

    public void Upgrade(string id)
    {
        if (id == "1")
        {
            if (Resources.Energy < 5)
            {
                return;
            }
            
            Resources.SetEnergy(-5);
            Resources.UpgradeHealth();
        }

        if (id == "2")
        {
            if (Resources.Energy < 5)
            {
                return;
            }

            Resources.SetEnergy(-5);
            Resources.UpgradeDamage();
        }

        if (id == "3")
        {
            if (Resources.Energy < 5)
            {
                return;
            }

            Resources.SetEnergy(-5);
            Resources.UpgradeDefense();
        }
    }
}
