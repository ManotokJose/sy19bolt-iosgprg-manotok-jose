﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceController : MonoBehaviour
{
    public static ResourceController Instance;

    public float PlayerDamage;
    public float PlayerDefense;
    public float MaxHealth;
    public float PlayerHealth;
    public float MaxEnergy;
    public float Energy;

    // Start is called before the first frame update
    void Start()
    {
        MaxEnergy = 100;
        MaxHealth = 100;

        Energy = MaxEnergy;
        PlayerHealth = MaxHealth;

        PlayerDamage = 10;
        PlayerDefense = .5f;
    }

    void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetEnergy(float OtherEnergy)
    {
        Energy += OtherEnergy;
    }

    public void SetHealth(float damage)
    {
        PlayerHealth -= damage - PlayerDefense;
    }

    public void UpgradeHealth()
    {
        PlayerHealth += 10;
        MaxHealth += 10;
    }

    public void UpgradeDamage()
    {
        PlayerDamage += 5;
    }

    public void UpgradeDefense()
    {
        PlayerDefense += 0.5f;
    }
}
