﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(NavMeshAgent))]

public class PlayerMain : MonoBehaviour
{
    ResourceController Resources;

    public GameObject lockedEnemy = null;

    public GameObject BuildButtons;
    public GameObject ShopButtons;
    public GameObject ExitButton;
    public GameObject CarryButton;
    public GameObject Upgrade1;
    public GameObject Upgrade2;
    public GameObject Upgrade3;

    public float PlayerAttackSpeed = 2;
    public float Timer = 0;
    public float MovementSpeed = 10;

    Rigidbody rb;

    private Vector3 targetPosition;

    const int LEFT_MOUSE_BUTTON = 0;

    NavMeshAgent agent;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        Resources = ResourceController.Instance;

        targetPosition = transform.position;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return;
            }

            SetPosition();
            agent.SetDestination(targetPosition);
        }

        if (lockedEnemy != null)
        {
            Timer += Time.deltaTime;

            if (Timer > PlayerAttackSpeed)
            {
                lockedEnemy.SendMessage("EnemySetHealth", Resources.PlayerDamage);
                Timer = 0;
            }
        }

        if (Resources.PlayerHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("enemy found");
            if (lockedEnemy == null)
            {
                lockedEnemy = collision.gameObject;
            }
        }

        if (collision.gameObject.CompareTag("Shop"))
        {
            CarryButton.SetActive(false);
            ExitButton.SetActive(false);
            BuildButtons.SetActive(false);
            ShopButtons.SetActive(true);
        }
        if (collision.gameObject.CompareTag("Spawn"))
        {
            CarryButton.SetActive(true);
            ExitButton.SetActive(false);
            BuildButtons.SetActive(false);
            ShopButtons.SetActive(false);

            Upgrade1.SetActive(false);
            Upgrade2.SetActive(false);
            Upgrade3.SetActive(false);
        }

        if(collision.gameObject.CompareTag("Exit"))
        {
            CarryButton.SetActive(false);
            ExitButton.SetActive(true);
            BuildButtons.SetActive(false);
            ShopButtons.SetActive(false);

            Upgrade1.SetActive(false);
            Upgrade2.SetActive(false);
            Upgrade3.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Room"))
        {
            CarryButton.SetActive(false);
            ExitButton.SetActive(false);
            BuildButtons.SetActive(true);
            ShopButtons.SetActive(false);

            Upgrade1.SetActive(false);
            Upgrade2.SetActive(false);
            Upgrade3.SetActive(false);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        //original code resets the timer, might cause a bug where player can attack too fast by making enemies go in and out of attack range
        if (collision.gameObject == lockedEnemy)
        {
            lockedEnemy = null;
        }
    }

    void SetPosition()
    {
        Plane plane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float point = 0f;
        if (plane.Raycast(ray, out point))
        {
            targetPosition = ray.GetPoint(point);
        }
    }
}
