﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public List<GameObject> Enemies;

    ResourceController Resources;
    GameObject BuildUI;
    GameObject EManager;
    
    public GameObject TowerLocation;
    public GameObject EnemyLocation;
    public GameObject Energy;

    public bool IsShop = false;
    public bool HasTower = false;
    public bool IsExplored = false;
    public bool IsPowered;
    public bool IsSpawnRoom;

    // Start is called before the first frame update
    void Start()
    {
        EManager = GameObject.FindGameObjectWithTag("EnemySpawnManager");
        BuildUI = GameObject.FindGameObjectWithTag("BuildUI");

        if (IsSpawnRoom)
        {
            IsPowered = true;
            IsExplored = true;
            Instantiate(Energy, TowerLocation.transform.position, Quaternion.identity);
        }

        if (IsShop)
        {
            IsPowered = true;
        }
    }

    // Update is called once per frame  
    void LateUpdate()
    {
        
    }

    public void PowerRoom()
    {
        if (IsPowered == true)
        {
            Resources.SetEnergy(15);
            IsPowered = false;
        }
        else
        {
            Resources.SetEnergy(-15);
            IsPowered = true;
        }
    }

    public void SpawnEnemy()
    {
        if (IsSpawnRoom || !IsExplored || IsPowered)
        {
            return;
        }

        int randomNumber = Random.Range(0, Enemies.Count);

        GameObject prefab = Enemies[randomNumber];

        Instantiate(prefab, EnemyLocation.transform.position, Quaternion.identity);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            BuildUI.SendMessage("SetTowerLocation", TowerLocation);
            if (!IsExplored)
            {
                EManager.SendMessage("SpawnRoomEnemies");
                IsExplored = true;
            }
        }
    }
}
