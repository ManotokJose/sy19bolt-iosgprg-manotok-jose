﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.iOS;
using UnityEngine;

public class AOETarget : BaseTowerScript
{

    public List<GameObject> currentHitObjects = new List<GameObject>();

    public float SphereRadius = 9;
    public float MaxDistance = 9;
    public LayerMask layermask;

    protected override void OnAttack()
    {
        base.OnAttack();
        currentHitObjects.Clear();

        RaycastHit[] hitColliders = Physics.SphereCastAll(transform.position, SphereRadius, Vector3.zero, 0, layermask, QueryTriggerInteraction.UseGlobal);
        foreach (RaycastHit hit in hitColliders)
        {
            currentHitObjects.Add(hit.transform.gameObject);
            EnemyBase enemy = hit.collider.gameObject.GetComponent<EnemyBase>();
            if (enemy != null)
            {
                GameObject bulletGameObject = Instantiate(bullets, this.transform.position, Quaternion.identity);
                Bullet bullet = bulletGameObject.GetComponent<Bullet>();
                bullet.EnemyTarget = LockedTarget.gameObject;
            }
        }
    }
}
