﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleTargetScript : BaseTowerScript
{
    public GameObject Bullets;

    protected override void OnAttack()
    {
        base.OnAttack();
        GameObject bulletGameObject =  Instantiate(bullets, this.transform.position, Quaternion.identity);
        Bullet bullet = bulletGameObject.GetComponent<Bullet>();
        bullet.EnemyTarget = LockedTarget.gameObject;
    }
}
