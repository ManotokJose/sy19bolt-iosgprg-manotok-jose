﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyTower : MonoBehaviour
{
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason
    //DOES NOT WORK for some reason

    ResourceController Resources;
    GameObject player;

    bool click = false;

    // Start is called before the first frame update
    void Start()
    {
        Resources = ResourceController.Instance;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (click == true)
        {
            transform.position = player.transform.position;
        }

        if (Resources.Energy <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void EnablePosition()
    {
        click = true;
    }
}
