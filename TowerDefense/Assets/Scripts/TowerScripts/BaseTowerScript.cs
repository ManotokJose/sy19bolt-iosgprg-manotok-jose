﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTowerScript : MonoBehaviour
{
    public GameObject LockedTarget = null;
    public GameObject bullets;
    public float Health = 40;
    public float Damage = 10;
    public float AttackSpeed = 3;
    public int PowerCost = 10;

    protected float Timer = 0;
    protected ResourceController Resources;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        Resources = ResourceController.Instance;
        Resources.SetEnergy(-PowerCost);
    }

    // Update is called once per frame
    void Update()
    {
        if (Health <= 0)
        {
            Resources.SetEnergy(PowerCost);
            Destroy(this.gameObject);
        }

        if (LockedTarget != null)
        {
            Timer += Time.deltaTime;

            if (Timer > AttackSpeed)
            {
                OnAttack();
                Timer = 0;
            }
        }

    }

    public void SetHealth(float damage)
    {
        Health -= damage;
    }

    protected virtual void OnTriggerEnter(Collider collision)
    {
        //does not work for whatever the reason
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (LockedTarget == null)
            {
                LockedTarget = collision.gameObject;
            }
        }
    }

    void OnTriggerExit(Collider collision)
    {
        //original code resets the timer, might cause a bug where player can attack too fast by making enemies go in and out of attack range

        if (LockedTarget == collision.gameObject)
        {
            LockedTarget = null;
        }
    }

    protected virtual void OnAttack()
    {

    }
}
