﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    float speed = 10;
    public GameObject EnemyTarget;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, EnemyTarget.transform.position, speed * Time.deltaTime);
        if (EnemyTarget == null)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        //does not work for whatever the reason
        if (collision.gameObject.CompareTag("EnemyBody"))
        {
            Destroy(this.gameObject);
        }
    }
}
