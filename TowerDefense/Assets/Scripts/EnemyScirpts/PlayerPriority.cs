﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.AI;

public class PlayerPriority : EnemyBase
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void LookForTarget()
    {
        base.LookForTarget();
        target = GameObject.FindGameObjectWithTag("Player");
    }

    protected override void OnAttack()
    {
        base.OnAttack();
        if (LockedTarget.gameObject.CompareTag("Player"))
        {
            Resources.SetHealth(Damage);
        }
    }
}
