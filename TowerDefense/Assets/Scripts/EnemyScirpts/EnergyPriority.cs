﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyPriority : EnemyBase
{
    protected override void Start()
    {
        base.Start();
    }

    protected override void LookForTarget()
    {
        base.LookForTarget();
        target = GameObject.FindGameObjectWithTag("Energy");
    }

    protected override void OnAttack()
    {
        base.OnAttack();
        if (LockedTarget.gameObject.CompareTag("Energy"))
        {
            Resources.SetEnergy(-Damage);
        }
    }
}
