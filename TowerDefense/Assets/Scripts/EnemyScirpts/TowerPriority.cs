﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPriority : EnemyBase
{
    protected override void Start()
    {
        base.Start();
        
    }

    protected override void LookForTarget()
    {
        base.LookForTarget();
        target = GameObject.FindGameObjectWithTag("Tower");
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player");
        }
    }

    protected override void OnAttack()
    {
        base.OnAttack();
        if (LockedTarget.gameObject.CompareTag("Player"))
        {
            Resources.SetHealth(Damage);
        }
        else
        {
            LockedTarget.gameObject.SendMessage("SetHealth", Damage);
        }
    }
}
