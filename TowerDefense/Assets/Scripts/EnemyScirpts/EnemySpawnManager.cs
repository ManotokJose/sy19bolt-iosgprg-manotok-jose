﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    GameObject[] Rooms;
    int NumberOfRooms;

    // Start is called before the first frame update
    void Start()
    {
        Rooms = GameObject.FindGameObjectsWithTag("Room");
        NumberOfRooms = GameObject.FindGameObjectsWithTag("Room").Length;
    }

    // Update is called once per frame
    void LateUpdate()
    {
    }

    public void SpawnRoomEnemies()
    {
        for (int i = 0; i < NumberOfRooms; i++)
        {
            Rooms[i].SendMessage("SpawnEnemy");
        }
    }
}
