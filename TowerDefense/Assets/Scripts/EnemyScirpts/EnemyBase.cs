﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBase : MonoBehaviour
{
    public GameObject target;
    
    public Transform LockedTarget = null;

    public float Health = 50;
    public float Damage = 10;
    public float AttackSpeed = 3;
    
    protected float Timer = 0;
    protected ResourceController Resources;

    NavMeshAgent agent;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        Resources = ResourceController.Instance;
    }

    protected virtual void Update()
    {
        if (target == null)
        {
            LookForTarget();
        }

        agent.SetDestination(target.transform.position);

        if (LockedTarget != null)
        {
            Timer += Time.deltaTime;

            if (Timer > AttackSpeed)
            {
                OnAttack();
                Timer = 0;
            }
        }

        if (Health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    public void EnemySetHealth(float Damage)
    {
        Health -= Damage;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            EnemySetHealth(10);
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            if (LockedTarget == null)
            {
                LockedTarget = collision.transform;
            }
        }

        if (collision.gameObject.CompareTag("Tower"))
        {
            agent.speed = agent.speed / 2;
            if (LockedTarget == null)
            {
                LockedTarget = collision.transform;
            } 
        }
    }

    void OnTriggerExit(Collider collision)
    {
        //original code resets the timer, might cause a bug where player can attack too fast by making enemies go in and out of attack range

        if (LockedTarget == collision.transform)
        {
            LockedTarget = null;
        }
    }

    protected virtual void OnAttack()
    {

    }

    protected virtual void LookForTarget()
    {

    }
}
