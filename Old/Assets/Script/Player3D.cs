﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

[RequireComponent(typeof(NavMeshAgent))]

//tageting code https://answers.unity.com/questions/709135/only-damage-one-object-with-enemy-tag.html

public class Player3D : MonoBehaviour
{
    public GameObject CarryEnergy;

    public float maxHealth = 100;
    public float playerHealth;

    Rigidbody rb;

    public float movementSpeed = 10;

    private Vector3 targetPosition;

    const int LEFT_MOUSE_BUTTON = 0;

    NavMeshAgent agent;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        targetPosition = transform.position;
        rb = GetComponent<Rigidbody>();

        playerHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        searchEnemy();
        if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
        {
            setPosition();
            agent.SetDestination(targetPosition);
        }
    }

    public void damagePlayer(float damage)
    {
        playerHealth -= damage;

        if (playerHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void setPosition()
    {
        //from what i understand
        //create plane
        Plane plane = new Plane(Vector3.up, transform.position);
        //Convert mouse to ray
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float point = 0f;

        //check if ray from mouse, intersects with plane
        //if the ray hits the plane convert point to vec3 target position
        if (plane.Raycast(ray, out point))
        {
            targetPosition = ray.GetPoint(point);
        }
    }

    void searchEnemy()
    {
        //need to optimize
        if ((GameObject.FindGameObjectsWithTag("Enemy").Length == 0))
        {
            healPlayer();
        }
    }

    void healPlayer()
    {
        playerHealth = maxHealth;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Energy"))
        {
            bool CarryEnergyState = CarryEnergy.activeSelf;
            CarryEnergy.SetActive(!CarryEnergyState);
        }
    }
}
