﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking.Match;

public class RoomCode : MonoBehaviour
{
    EnergyScript energy;
    public List<GameObject> EnemySpawn;

    public GameObject EnemyLocation;
    public GameObject EnergyLocation;
    public GameObject RoomUI;
    public GameObject Energy;
    public GameObject BuildSlot;

    public bool isExplored = false;
    public bool isSpawnRoom;

    public bool isPowered = false;
    bool isPlayerInRoom;

    public bool hasBuildModule;

    float BaseEnergyPercent = 15;

    public void PowerRoom()
    {
        if (isPowered == false)
        {
            isPowered = true;
        }
        else
        {
            isPowered = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        if (isSpawnRoom)
        {
            Instantiate(Energy, EnemyLocation.transform.position, Quaternion.identity);
            isPowered = true;
            isExplored = true;
        }

        if (hasBuildModule == false)
        {
            BuildSlot.SetActive(false);
        }

        //int randomNumber = Random.Range(0, 1);
        //if (randomNumber == 0)
        //{
        //    BuildSlot.SetActive(true);
        //}
        //else
        //{
        //    BuildSlot.SetActive(false);
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (RoomUI != null)
        {
            RoomUI.SetActive(isPlayerInRoom);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        //maybe use foreach later(?) for each powered room, spawn enemy? balance? 何か
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerInRoom = true;
            if (!isExplored)
            {
                //float RandomEnergyPercent = Random.Range(0, 100);
                //if (RandomEnergyPercent > BaseEnergyPercent)
                //{
                //    energy.Energy += 10;
                //    energy.MaxEnergy += 10;
                //}

                int randomNumber = Random.Range(0, EnemySpawn.Count);

                GameObject prefab = EnemySpawn[randomNumber];

                Instantiate(prefab, EnemyLocation.transform.position, Quaternion.identity);

                isExplored = true;
            }
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerInRoom = false;
        }
    }
}
