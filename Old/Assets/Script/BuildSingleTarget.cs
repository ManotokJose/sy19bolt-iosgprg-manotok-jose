﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildSingleTarget : MonoBehaviour
{

    public GameObject tower;
    public void buildTower()
    {
        if (tower != null)
        {
            bool isActive = tower.activeSelf;
            tower.SetActive(!isActive);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
