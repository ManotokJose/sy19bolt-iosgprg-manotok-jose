﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyPlayerPrio : MonoBehaviour
{
    public Player3D player;

    GameObject target;

    public float movementSpeed = 6;
    public float health = 50;
    public int damage = 5;
    public float attackSpeed = 3;
    float time = 0;

    private Vector3 targetPosition;

    NavMeshAgent agent;

    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (target == null)
        {
            return;
        }
        agent.SetDestination(target.transform.position);
    }

    public void damageEnemy(float playerDamage)
    {
        health -= playerDamage;

        Debug.Log("enemyhit");
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            time += Time.deltaTime;
            if (time > attackSpeed)
            {
                target.transform.SendMessage("setHealth", damage);
                time = 0;
            }
        }
    }
}
