﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyScript : MonoBehaviour
{
    public int Energy;
    public int MaxEnergy;

    public GameObject CarryEnergyUI;
    GameObject playerPosition;

    bool EnableMovement = false;

    //Start is called before the first frame update
    void Start()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player");
        Energy = 100;
        MaxEnergy = 100;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (EnableMovement == true)
        {
            this.transform.position = playerPosition.transform.position;
        }
    }

    public void SetPosition()
    {
        EnableMovement = true ;
    }

    private void OnTriggerEnter(Collider collision)
    {
        //maybe use foreach later(?) for each powered room, spawn enemy? balance? 何か
        if (collision.gameObject.CompareTag("Player"))
        {
            CarryEnergyUI.SetActive(true);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            CarryEnergyUI.SetActive(false);
        }
    }

    public void setHealth(int damage)
    {
        Energy -= damage;

        Debug.Log("energyhit");
        if (Energy <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
