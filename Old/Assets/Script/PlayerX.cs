﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

//ref https://www.youtube.com/watch?v=rWmLCerzio8
[RequireComponent(typeof(NavMeshAgent))]
public class Player : MonoBehaviour
{
    //ref https://www.youtube.com/watch?v=Cw6B-VYNba0

    public float movementSpeed = 5;

    NavMeshAgent agent;

    Rigidbody2D rb;

    Touch touch;

    Vector3 touchPosition;
    Vector3 whereToMove;

    bool isMoving = false;

    float previousDistamceToTouchPos, currentDistanceToTouchPos;

    // Start is called before the first frame update
    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        whereToMove = transform.position;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //for touch

        if (isMoving)
        {
            currentDistanceToTouchPos = (touchPosition - transform.position).magnitude;
        }

        //testing
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            previousDistamceToTouchPos = 0;
            currentDistanceToTouchPos = 0;
            isMoving = true;
            touchPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            touchPosition.z = 0;
            whereToMove = (touchPosition - transform.position).normalized;
            agent.SetDestination(whereToMove);
            rb.velocity = new Vector2(whereToMove.x * movementSpeed, whereToMove.y * movementSpeed);
        }

        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                previousDistamceToTouchPos = 0;
                currentDistanceToTouchPos = 0;
                isMoving = true;
                touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                touchPosition.z = 0;
                whereToMove = (touchPosition - transform.position).normalized;
                rb.velocity = new Vector2(whereToMove.x * movementSpeed, whereToMove.y * movementSpeed);
            }
        }

        if (currentDistanceToTouchPos > previousDistamceToTouchPos)
        {
            isMoving = false;
            rb.velocity = Vector2.zero;
        }

        if (isMoving)
        {
            previousDistamceToTouchPos = (touchPosition - transform.position).magnitude;
        }
    }
}
