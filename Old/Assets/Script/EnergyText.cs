﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyText : MonoBehaviour
{
    EnergyScript Energy;

    public Text text;

    void Update()
    {
        text.text = "Energy: " + Energy.Energy.ToString();
    }
}