﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyEnergyAi : MonoBehaviour
{
    Rigidbody rb;

    public float movementSpeed = 6;
    public float health = 50;
    public int damage = 1;
    public float attackSpeed = 3;
    float time = 0;

    private Vector3 targetPosition;

    GameObject target;

    NavMeshAgent agent;

    public void damageEnemy(float playerDamage)
    {
        health -= playerDamage;

        Debug.Log("enemyhit");
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Energy");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        agent.SetDestination(target.transform.position);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Energy"))
        {
            time += Time.deltaTime;
            if (time > attackSpeed)
            {
                target.transform.SendMessage("setHealth", damage);
                time = 0;
            }
        }
    }
}
