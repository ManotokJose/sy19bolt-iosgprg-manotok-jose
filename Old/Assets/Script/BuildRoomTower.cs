﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildRoomTower : MonoBehaviour
{
    public GameObject TowerLocation;


    public GameObject SingleTarget;
    public GameObject AOETarget;

    bool TowerIsBuilt = false;

    public void BuildSingleTarget()
    {
        if (TowerIsBuilt == false)
        {
            Instantiate(SingleTarget, TowerLocation.transform.position, Quaternion.identity);
            TowerIsBuilt = true;
        }
    }

    public void BuildAOETower()
    {
        if (TowerIsBuilt == false)
        {
            Instantiate(AOETarget, TowerLocation.transform.position, Quaternion.identity);
            TowerIsBuilt = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

}
