﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerRoom : MonoBehaviour
{
    //text
    public GameObject powerON;
    public GameObject powerOFF;

    public GameObject buildOptions;

    public void loadPowerOptions()
    {
        bool textButtonPowerOFF = powerOFF.activeSelf;
        powerOFF.SetActive(!textButtonPowerOFF);

        bool textButtonPowerON = powerON.activeSelf;
        powerON.SetActive(!textButtonPowerON);

        if (buildOptions != null)
        {
            bool isActive = buildOptions.activeSelf;
            buildOptions.SetActive(!isActive);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
