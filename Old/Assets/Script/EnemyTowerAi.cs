﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyTowerAi : MonoBehaviour
{
    Rigidbody rb;

    public float movementSpeed = 6;
    public float health = 50;
    public int damage = 10;
    public float attackSpeed = 3;
    float time = 0;

    private Vector3 targetPosition;
    
    GameObject target;

    NavMeshAgent agent;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }
    public void damageEnemy(float playerDamage)
    {
        health -= playerDamage;

        Debug.Log("enemyhit");
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Tower");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        agent.SetDestination(target.transform.position);
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Tower");
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Tower"))
        {
            time += Time.deltaTime;
            if (time > attackSpeed)
            {
                target.transform.SendMessage("setHealth", damage);
                time = 0;
            }
        }
    }
}
