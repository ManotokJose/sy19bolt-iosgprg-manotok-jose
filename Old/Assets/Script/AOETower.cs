﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOETower : MonoBehaviour
{
    List<Transform> enemies = new List<Transform>();

    public int TowerAttack = 10;
    float attackSpeed = 3;
    float time = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (time > attackSpeed)
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].gameObject.SendMessage("damageEnemy", TowerAttack);
            }
            time = 0;
        }

        time += Time.deltaTime;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            enemies.Add(collision.transform);
        }
    }
}