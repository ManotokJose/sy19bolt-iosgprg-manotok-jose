﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleTowerScript : MonoBehaviour
{
    List<Transform> enemies = new List<Transform>();
    public Transform lockedEnemy = null;

    public int TowerHealth = 30;

    public float Tower = 10;
    float attackSpeed = 3;
    float time = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (lockedEnemy != null)
        {
            time += Time.deltaTime;

            if (time > attackSpeed)
            {
                lockedEnemy.gameObject.SendMessage("damageEnemy", Tower);
                time = 0;
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (lockedEnemy == null)
            {
                lockedEnemy = collision.transform;
            }

            enemies.Add(collision.transform);
        }
    }

    void OnTriggerLeave(Collider collision)
    {
        //original code resets the timer, might cause a bug where player can attack too fast by making enemies go in and out of attack range

        if (lockedEnemy == collision.transform)
        {
            lockedEnemy = null;
        }
        enemies.Remove(collision.transform);

        if (enemies.Count > 0)
        {
            lockedEnemy = enemies[0];
        }
        else
        {
            lockedEnemy = null;
        }
    }

    public void setHealth(int damage)
    {
        TowerHealth -= damage;

        Debug.Log("towerhit");
        if (TowerHealth <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
