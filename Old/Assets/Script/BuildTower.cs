﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTower : MonoBehaviour
{
    RoomCode room;

    public GameObject SingleTarget;
    public GameObject AOETarget;

    //test
    public void loadBuildOptions()
    {
        if (SingleTarget != null)
        {
            bool isActive = SingleTarget.activeSelf;
            SingleTarget.SetActive(!isActive);
        }

        if (AOETarget != null)
        {
            bool isActive = AOETarget.activeSelf;
            AOETarget.SetActive(!isActive);
        }

    }

    //public void loadBuildOptions()
    //{
    //    if (room.hasBuildModule == false)
    //    {
    //        SingleTarget.SetActive(false);
    //        AOETarget.SetActive(false);
    //    }
    //    else
    //    {
    //        if (SingleTarget != null)
    //        {
    //            bool isActive = SingleTarget.activeSelf;
    //            SingleTarget.SetActive(!isActive);
    //        }
    //
    //        if (AOETarget != null)
    //        {
    //            bool isActive = AOETarget.activeSelf;
    //            AOETarget.SetActive(!isActive);
    //        }
    //    }
    //}


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
